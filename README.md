# mafia

Explicacion de modelado y Arquitectura de la aplicacion en docuemnto PDF.

API rest mediante uso de Flask y python, con persitencia en mongoDB:

Software necesario:
python 3.3
pip
MongoDB 4.2.0(Previa creacion de database mafia)
git

Debido a la falta de tiempos no se facilita el docker-compose que permita levantar la aplicacion e incializar la misma, aunque no se pida en el enunciado seria intersante desarrollarlo a futuro con mas tiempo. 

Aranque:
clone repo
pip install requirements.txt
python app.py

**URLS**

''' Metodo que devuelve todos aquellos jefes que tengan mas de 50 esbirros a su cargo '''
http://localhost:5000/mafias_max

'''
Devuelve todos los jefes existentes en la base de datos
'''
http://localhost:5000/all_boss

'''
Devuelve todos los esbirros existentes en la base de datos
'''
http://localhost:5000/all_esbirros

'''
Devuelve todos las fechorias existentes en la base de datos
'''
http://localhost:5000/all_crimes

''' Llamada raiz que devuelve por defecto todos los jefes'''
http://localhost:5000/

''' Metodo para añadir un nuevo esbirro '''
http://localhost:5000/add_esbirro

''' Metodo para añadir un nuevo jefe '''
http://localhost:5000/add_jefe

  ''' Metodo para añadir nuevas fechorias para un  '''
http://localhost:5000/add_crimen


La relacion entre las distintas collecciones se realiza mediante ids, a futuro se podria desarrollar un forntal web que visualizara las mismas de una manera mas visual.

