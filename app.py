# app.py

from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo
import json
from bson import json_util

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'mafia'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/mafia'

mongo = PyMongo(app)

'''
Devuelve todos los jefes existentes en la base de datos
'''
@app.route('/all_boss',methods=['GET'])
def todoBoss():
  jefes = mongo.db.jefes
  output = []
  for s in jefes.find():
    output.append({'name' : s['name'],
     'clan' : s['clan'],
     'status' : s['status'],
     'category' : s['category'],
     'clan' : s['clan'],
     'esmirros' : s['esmirros'],
     'fechorias' : s['fechorias'],
     'id' : s['id']})
  return jsonify({'result' : output})

'''
Devuelve todos los esbirros existentes en la base de datos
'''
@app.route('/all_esbirros',methods=['GET'])
def todoEsbirros():
  esbirros = mongo.db.esbirros
  output = []
  for s in esbirros.find():
    output.append({'name' : s['name'],
     'clan' : s['clan'],
     'clan' : s['clan'],
     'boss' : s['boss'],
     'fechorias' : s['fechorias'],
     'id' : s['id']})
  return jsonify({'result' : output})

'''
Devuelve todos las fechorias existentes en la base de datos
'''
@app.route('/all_crimes',methods=['GET'])
def todoFechorias():
  fechorias = mongo.db.fechorias
  output = []
  for s in fechorias.find():
    output.append({'name' : s['name'],
     'date' : s['date'],
     'daños' : s['daños'],
     'esbirros' : s['esbirros'],
     'id' : s['id']})
  return jsonify({'result' : output})

''' Llamada raiz que devuelve por defecto todos los jefes'''
@app.route('/',methods=['GET'])
def todo():
  jefes = mongo.db.jefes
  _items = jefes.find()
  items = [item for item in _items]

  return jsonify({'jefes' : json.dumps(items, default=json_util.default)})

''' Metodo para añadir un nuevo esbirro '''
@app.route("/add_esbirro", methods = ['POST'])
def add_esbirro():
  esbirros = mongo.db.esbirros
  json = request.get_json()
  if not json:
    abort(400)
  user_name = json['name']
  category = json['category']
  clan = json['clan']
  boss = json['boss']
  fechorias = json['fechorias']
  idd = json['id']
  status = esbirros.insert_one({
  "name" : user_name,
  "clan" : clan,
  "category" : category,
  "boss" : boss,
  "fechorias" : fechorias,
  "id" : idd
  })
  return jsonify({'message' : 'SUCCESS'})

''' Metodo para añadir un nuevo jefe '''
@app.route("/add_jefe", methods = ['POST'])
def add_jefe():
  jefes = mongo.db.jefes
  json = request.get_json()
  if not json:
    abort(400)
  user_name = json['name']
  category = json['category']
  status = json['status']
  clan = json['clan']
  esmirros = json['esmirros']
  fechorias = json['fechorias']
  idd = json['id']
  status = jefes.insert_one({
  "name" : user_name,
  "clan" : clan,
  "status" : status,
  "category" : category,
  "esmirros" : esmirros,
  "fechorias" : fechorias,
  "id" : idd
  })
  return jsonify({'message' : 'SUCCESS'})

  ''' Metodo para añadir nuevas fechorias para un  '''
@app.route("/add_crimen", methods = ['POST'])
def add_crimen():
  fechorias = mongo.db.fechorias
  json = request.get_json()
  if not json:
    abort(400)
  name_crime = json['name']
  date = json['date']
  danios = json['danios']
  esbirros = json['esbirros']
  idd = json['id']
  status = fechorias.insert_one({
  "name" : name_crime,
  "date" : date,
  "daños" : danios,
  "esbirros" : esbirros,
  "id" : idd
  })
  return jsonify({'message' : 'SUCCESS'})

''' Metodo que devulee todos aquellos jefes que tengan mas de 50 esbirros a su cargo '''
@app.route("/mafias_max", methods = ['GET'])
def mafias_max():
  jefes = mongo.db.jefes
  output = []
  expres = {"$expr":{"$gt":[{"$size":{"$ifNull":["$esmirros", []]}}, 1]}}
  #page_sanitized = json.loads(json_util.dumps(expres))
  for s in jefes.find(expres):
    output.append({'name' : s['name'],
     'clan' : s['clan'],
     'status' : s['status'],
     'category' : s['category'],
     'clan' : s['clan'],
     'esmirros' : s['esmirros'],
     'fechorias' : s['fechorias'],
     'id' : s['id']})
  return jsonify({'result' : output})


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
